import React, {Component} from 'react'

class Header extends Component{
	
	componentDidMount () {
		const script = document.createElement("script");
		script.text = "$(document).ready(function(){ $('.modal').modal(); $('.modal').modal('open'); $('select').formSelect(); $('.fixed-action-btn').floatingActionButton(); })";
		document.body.appendChild(script);
	}

	render() {
		return (
			<head>
				<meta charSet="utf-8"/>
				<meta name="viewport" content="width=device-width, initial-scale=1" />
				<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"/>
				<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
				<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
				<title>Plague Inc</title>
			</head>
		);
	}

}

export default Header;
