webpackHotUpdate(3,{

/***/ "./pages/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(module) {

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__("./node_modules/react/cjs/react.development.js");

var _react2 = _interopRequireDefault(_react);

var _reactSimpleMaps = __webpack_require__("./node_modules/react-simple-maps/lib/index.js");

var _chromaJs = __webpack_require__("./node_modules/chroma-js/chroma.js");

var _chromaJs2 = _interopRequireDefault(_chromaJs);

var _d3Scale = __webpack_require__("./node_modules/d3-scale/index.js");

var _axios = __webpack_require__("./node_modules/axios/index.js");

var _axios2 = _interopRequireDefault(_axios);

var _header = __webpack_require__("./pages/header.js");

var _header2 = _interopRequireDefault(_header);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(function () {
	var enterModule = __webpack_require__("./node_modules/react-hot-loader/index.js").enterModule;

	enterModule && enterModule(module);
})();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var wrapperStyles = {
	width: "100%",
	maxWidth: 1200,
	margin: "0 auto"
};

var cityScale = (0, _d3Scale.scaleLinear)().domain([0, 37843000]).range([1, 25]);

var colorScale = _chromaJs2.default.scale(['#FF6E40', 'FFD740', '#00B8D4']).mode('lch').colors(24);

var subregions = ["Southern Asia", "Polynesia", "Micronesia", "Southern Africa", "Central Asia", "Melanesia", "Western Europe", "Central America", "Seven seas (open ocean)", "Northern Africa", "Caribbean", "South-Eastern Asia", "Eastern Africa", "Australia and New Zealand", "Eastern Europe", "Western Africa", "Southern Europe", "Eastern Asia", "South America", "Middle Africa", "Antarctica", "Northern Europe", "Northern America", "Western Asia"];

var popScale = (0, _d3Scale.scaleLinear)().domain([0, 100000000, 1400000000]).range(["#CFD8DC", "#607D8B", "#37474F"]);

var UpdatableChoropleth = function (_Component) {
	_inherits(UpdatableChoropleth, _Component);

	function UpdatableChoropleth() {
		_classCallCheck(this, UpdatableChoropleth);

		var _this = _possibleConstructorReturn(this, (UpdatableChoropleth.__proto__ || Object.getPrototypeOf(UpdatableChoropleth)).call(this));

		_this.state = {
			populationData: false,
			cities: [],
			plague_name: 'PLG-007',
			continent: 'Asia',
			plague_X: 100,
			plague_Y: 40,
			plague_Z: 15,
			africa_infection: 0,
			america_infection: 0,
			europa_infection: 0,
			asia_infection: 0
		};

		_this.switchToPopulation = _this.switchToPopulation.bind(_this);
		_this.switchToRegions = _this.switchToRegions.bind(_this);
		_this.fetchCities = _this.fetchCities.bind(_this);
		_this.updatePlagueName = _this.updatePlagueName.bind(_this);
		_this.updateContinent = _this.updateContinent.bind(_this);
		return _this;
	}

	_createClass(UpdatableChoropleth, [{
		key: "switchToPopulation",
		value: function switchToPopulation() {
			this.setState({ populationData: true });
		}
	}, {
		key: "switchToRegions",
		value: function switchToRegions() {
			this.setState({ populationData: false });
		}
	}, {
		key: "componentDidMount",
		value: function componentDidMount() {
			this.fetchCities();
		}
	}, {
		key: "updatePlagueName",
		value: function updatePlagueName(e) {
			this.setState({ plague_name: e.target.value });
		}
	}, {
		key: "updateContinent",
		value: function updateContinent(e) {
			this.setState({ continent: e.target.value });
			switch (e.target.value) {
				case 'Asia':
					this.setState({ plague_X: 100, plague_Y: 40 });
					break;
				case 'America':
					this.setState({ plague_X: -100, plague_Y: 40 });
					break;
				case 'Africa':
					this.setState({ plague_X: 10, plague_Y: 20 });
					break;
				case 'Europa':
					this.setState({ plague_X: 10, plague_Y: 50 });
					break;
				case 'Oceania':
					this.setState({ plague_X: 130, plague_Y: -20 });
					break;
			}
		}
	}, {
		key: "fetchCities",
		value: function fetchCities() {
			var _this2 = this;

			_axios2.default.get("/static/world-most-populous-cities.json").then(function (res) {
				_this2.setState({
					cities: res.data
				});
			});
		}
	}, {
		key: "render",
		value: function render() {
			var _this3 = this;

			return _react2.default.createElement(
				"div",
				null,
				_react2.default.createElement(_header2.default, null),
				_react2.default.createElement(
					"div",
					null,
					_react2.default.createElement(
						"h5",
						null,
						"Plaga -> ",
						this.state.plague_name
					),
					_react2.default.createElement(
						"p",
						null,
						"Continente -> ",
						this.state.continent
					),
					_react2.default.createElement(
						"p",
						null,
						"Fecha -> "
					),
					_react2.default.createElement(
						"p",
						null,
						"DNA -> "
					),
					_react2.default.createElement(
						"p",
						null,
						"Cure -> "
					),
					_react2.default.createElement(
						"p",
						null,
						"Severity -> "
					),
					_react2.default.createElement(
						"p",
						null,
						"Lethality -> "
					),
					_react2.default.createElement(
						"p",
						null,
						"Efectability -> "
					)
				),
				_react2.default.createElement(
					"div",
					{ id: "Welcome", className: "modal" },
					_react2.default.createElement(
						"div",
						{ className: "modal-content" },
						_react2.default.createElement(
							"h4",
							null,
							"Plague Inc"
						),
						_react2.default.createElement(
							"div",
							{ className: "row" },
							_react2.default.createElement(
								"form",
								{ className: "col s12" },
								_react2.default.createElement(
									"div",
									{ className: "row" },
									_react2.default.createElement(
										"div",
										{ className: "input-field col s6" },
										_react2.default.createElement("input", { placeholder: "PLG-007", id: "plague_name", type: "text", className: "validate", onChange: this.updatePlagueName }),
										_react2.default.createElement(
											"label",
											{ "for": "plague_name" },
											"Nombre de la plaga"
										)
									),
									_react2.default.createElement(
										"div",
										{ "class": "input-field col s6" },
										_react2.default.createElement(
											"select",
											{ id: "continent", onChange: this.updateContinent },
											_react2.default.createElement(
												"option",
												{ value: "Asia", selected: true },
												"Asia"
											),
											_react2.default.createElement(
												"option",
												{ value: "America" },
												"America"
											),
											_react2.default.createElement(
												"option",
												{ value: "Africa" },
												"Africa"
											),
											_react2.default.createElement(
												"option",
												{ value: "Europa" },
												"Europa"
											),
											_react2.default.createElement(
												"option",
												{ value: "Oceania" },
												"Oceania"
											)
										),
										_react2.default.createElement(
											"label",
											null,
											"Seleccione continente"
										)
									)
								)
							)
						)
					),
					_react2.default.createElement(
						"div",
						{ className: "modal-footer" },
						_react2.default.createElement(
							"button",
							{ className: "modal-close waves-effect waves-green btn-flat" },
							"Empezar"
						)
					)
				),
				_react2.default.createElement(
					"div",
					{ style: wrapperStyles },
					_react2.default.createElement(
						_reactSimpleMaps.ComposableMap,
						{
							projectionConfig: {
								scale: 205,
								rotation: [-11, 0, 0]
							},
							width: 980,
							height: 551,
							style: {
								width: "100%",
								height: "auto"
							}
						},
						_react2.default.createElement(
							_reactSimpleMaps.ZoomableGroup,
							{ center: [0, 20] },
							_react2.default.createElement(
								_reactSimpleMaps.Geographies,
								{
									geography: "/static/world-50m-with-population.json",
									disableOptimization: true
								},
								function (geographies, projection) {
									return geographies.map(function (geography, i) {
										return _react2.default.createElement(_reactSimpleMaps.Geography, {
											key: geography.properties.iso_a3 + "-" + i,
											cacheId: geography.properties.iso_a3 + "-" + i,
											geography: geography,
											projection: projection,
											onClick: _this3.handleClick,
											round: true,
											style: {
												default: {
													fill: _this3.state.populationData ? popScale(geography.properties.pop_est) : colorScale[subregions.indexOf(geography.properties.subregion)],
													stroke: "#607D8B",
													strokeWidth: 0.75,
													outline: "none"
												},
												hover: {
													fill: _this3.state.populationData ? "#263238" : (0, _chromaJs2.default)(colorScale[subregions.indexOf(geography.properties.subregion)]).darken(0.5),
													stroke: "#607D8B",
													strokeWidth: 0.75,
													outline: "none"
												},
												pressed: {
													fill: _this3.state.populationData ? "#263238" : (0, _chromaJs2.default)(colorScale[subregions.indexOf(geography.properties.subregion)]).brighten(0.5),
													stroke: "#607D8B",
													strokeWidth: 0.75,
													outline: "none"
												}
											}
										});
									});
								}
							),
							_react2.default.createElement(
								_reactSimpleMaps.Markers,
								null,
								_react2.default.createElement(
									_reactSimpleMaps.Marker,
									{ marker: { coordinates: [5, 47.3] } },
									_react2.default.createElement("circle", { cx: 0, cy: 150, r: 5 })
								),
								_react2.default.createElement(
									_reactSimpleMaps.Marker,
									{ marker: { coordinates: [-65, 80] } },
									_react2.default.createElement("circle", { cx: 0, cy: 150, r: 5 })
								),
								_react2.default.createElement(
									_reactSimpleMaps.Marker,
									{ marker: { coordinates: [0, 90] } },
									_react2.default.createElement("circle", { cx: 0, cy: 150, r: 5 })
								),
								_react2.default.createElement(
									_reactSimpleMaps.Marker,
									{ marker: { coordinates: [100, 0] } },
									_react2.default.createElement("circle", { cx: 0, cy: 10, r: 5 })
								)
							),
							_react2.default.createElement(
								_reactSimpleMaps.Markers,
								null,
								_react2.default.createElement(
									_reactSimpleMaps.Marker,
									{ marker: { coordinates: [10, 20] } },
									_react2.default.createElement("circle", { cx: 0, cy: 10, r: 5, fill: "rgba(255,87,34,0.8)", stroke: "#607D8B", strokeWidth: "2" })
								),
								_react2.default.createElement(
									_reactSimpleMaps.Marker,
									{ marker: { coordinates: [-100, 40] } },
									_react2.default.createElement("circle", { cx: 0, cy: 10, r: 5, fill: "rgba(255,87,34,0.8)", stroke: "#607D8B", strokeWidth: "2" })
								),
								_react2.default.createElement(
									_reactSimpleMaps.Marker,
									{ marker: { coordinates: [10, 50] } },
									_react2.default.createElement("circle", { cx: 0, cy: 10, r: 5, fill: "rgba(255,87,34,0.8)", stroke: "#607D8B", strokeWidth: "2" })
								),
								_react2.default.createElement(
									_reactSimpleMaps.Marker,
									{ marker: { coordinates: [100, 40] } },
									_react2.default.createElement("circle", { cx: 0, cy: 10, r: 5, fill: "rgba(255,87,34,0.8)", stroke: "#607D8B", strokeWidth: "2" })
								),
								_react2.default.createElement(
									_reactSimpleMaps.Marker,
									{ marker: { coordinates: [this.state.plague_X, this.state.plague_Y] } },
									_react2.default.createElement("circle", { cx: 0, cy: 10, r: this.state.plague_Z, fill: "rgba(255,87,34,0.8)", stroke: "#607D8B", strokeWidth: "2" })
								)
							),
							_react2.default.createElement(
								_reactSimpleMaps.Annotation,
								{
									dx: 10,
									dy: -30,
									subject: [10, 18],
									strokeWidth: 2,
									stroke: "#607D8B"
								},
								_react2.default.createElement(
									"text",
									null,
									this.state.africa_infection,
									"%"
								)
							),
							_react2.default.createElement(
								_reactSimpleMaps.Annotation,
								{
									dx: 10,
									dy: -30,
									subject: [-100, 38],
									strokeWidth: 2,
									stroke: "#607D8B"
								},
								_react2.default.createElement(
									"text",
									null,
									this.state.america_infection,
									"%"
								)
							),
							_react2.default.createElement(
								_reactSimpleMaps.Annotation,
								{
									dx: 10,
									dy: -30,
									subject: [10, 48],
									strokeWidth: 2,
									stroke: "#607D8B"
								},
								_react2.default.createElement(
									"text",
									null,
									this.state.europa_infection,
									"%"
								)
							),
							_react2.default.createElement(
								_reactSimpleMaps.Annotation,
								{
									dx: 10,
									dy: -30,
									subject: [100, 38],
									strokeWidth: 2,
									stroke: "#607D8B"
								},
								_react2.default.createElement(
									"text",
									null,
									this.state.asia_infection,
									"%"
								)
							)
						)
					)
				)
			);
		}
	}, {
		key: "__reactstandin__regenerateByEval",
		value: function __reactstandin__regenerateByEval(key, code) {
			this[key] = eval(code);
		}
	}]);

	return UpdatableChoropleth;
}(_react.Component);

var _default = UpdatableChoropleth;
exports.default = _default;
;

(function () {
	var reactHotLoader = __webpack_require__("./node_modules/react-hot-loader/index.js").default;

	var leaveModule = __webpack_require__("./node_modules/react-hot-loader/index.js").leaveModule;

	if (!reactHotLoader) {
		return;
	}

	reactHotLoader.register(wrapperStyles, "wrapperStyles", "C:/Users/Shocklogic/Desktop/react-simple-maps-master/examples/switching-between-datasets/pages/index.js");
	reactHotLoader.register(cityScale, "cityScale", "C:/Users/Shocklogic/Desktop/react-simple-maps-master/examples/switching-between-datasets/pages/index.js");
	reactHotLoader.register(colorScale, "colorScale", "C:/Users/Shocklogic/Desktop/react-simple-maps-master/examples/switching-between-datasets/pages/index.js");
	reactHotLoader.register(subregions, "subregions", "C:/Users/Shocklogic/Desktop/react-simple-maps-master/examples/switching-between-datasets/pages/index.js");
	reactHotLoader.register(popScale, "popScale", "C:/Users/Shocklogic/Desktop/react-simple-maps-master/examples/switching-between-datasets/pages/index.js");
	reactHotLoader.register(UpdatableChoropleth, "UpdatableChoropleth", "C:/Users/Shocklogic/Desktop/react-simple-maps-master/examples/switching-between-datasets/pages/index.js");
	reactHotLoader.register(_default, "default", "C:/Users/Shocklogic/Desktop/react-simple-maps-master/examples/switching-between-datasets/pages/index.js");
	leaveModule(module);
})();

;
    (function (Component, route) {
      if(!Component) return
      if (false) return
      module.hot.accept()
      Component.__route = route

      if (module.hot.status() === 'idle') return

      var components = next.router.components
      for (var r in components) {
        if (!components.hasOwnProperty(r)) continue

        if (components[r].Component.__route === route) {
          next.router.update(r, Component)
        }
      }
    })(typeof __webpack_exports__ !== 'undefined' ? __webpack_exports__.default : (module.exports.default || module.exports), "/")
  
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__("./node_modules/next/node_modules/webpack/buildin/module.js")(module)))

/***/ })

})
//# sourceMappingURL=3.38fdb53db3c0cd9735e8.hot-update.js.map